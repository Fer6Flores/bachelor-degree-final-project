%Par�metros relativos a la Zynq-7000%
%----------------------------------------------------------------------------------%
fs = 1e4;                       %Frecuencia de muestreo en Hz
Ts = 1 / fs;                    %Per�odo de muestreo en s
f_GCLK = 100e6;                 %Frecuencia del reloj de la PL de la Zynq-7000
T_GCLK = 1 / f_GCLK;            %Per�odo del reloj de la PL de la Zynq-7000
counter_max =(Ts / T_GCLK);     %Valor l�mite del contador
ADC_max = 4095;                 %Valor m�ximo devuelto por el ADC
%----------------------------------------------------------------------------------%

%Matrices de la transformada dq%
%----------------------------------------------------------------------------------%
angulo1 = [0 1 -1] * 2*pi/3;
Talfabeta = 2/3 * [cos(angulo1) ; sin(angulo1)];    %Transformada de Clarke
angulo2 = [0 ; 1 ; -1] * 2*pi/3;
Tabc = [cos(angulo2) sin(angulo2)];                 %Transformada de Clarke inversa
%----------------------------------------------------------------------------------%
 
%Datos de la planta%
%----------------------------------------------------------------------------------%
f = 50;                     %Frecuencia de la red en Hz 
w = 2 * pi * f;             %Frecuencia de la red en rad/s
L = 12.5e-3;                %Inductancia da carga en H
R = 2.2;                    %Resistencia da carga en ohmnios
Udc = 700;                  %Tensi�n de entrada del convertidor 
Td = 1.5 / fs;              %Retardo debido a la actuaci�n del PWM
%----------------------------------------------------------------------------------%

%Par�metros del control de corriente en marco s�ncrono%
%----------------------------------------------------------------------------------%
alfa_damp = (6 - 4 * sqrt(2)) / Td;     %Sobreamortiguado
alfa_10 = 2 * pi * fs / 10;             %Subamortiguado
Kp = alfa_damp * L;                     %Kp del PI del control de corriente
Ki = alfa_damp * R;                     %Ki del PI del control de corriente
%----------------------------------------------------------------------------------%
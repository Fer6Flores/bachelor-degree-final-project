<h1>Code generation through Simulink Coder to control an inverter with a Zynq-7000 and its comparative with a F28M366 microcontroller</h1>

This is my final project for the bachelor's degree in industrial electronics and automation engineering and my tutors were Óscar López Sánchez and Luís Jacobo Álvarez Ruíz de Ojeda. The obtained grade with this project was a 9,2.

<h2>A brief introduction</h2>
The field this final degree work deals with is about power electronics, specifically it is about the control of grid converters.
The reduction of emissions of greenhouse gases for the mitigation of the global warning is a actual challenge. The use  of renewable energies is a useful strategy for the reduction of the fossil fuel consumption, wich are the higher responsables of this emissions. The connection of renewables energies, like photovoltaic and wind, to the grid requires the use of power converters. 

The control of this converters is done, currently, with specific microcontrollers like F28M366. This microcontrollers, which have specific hardware that makes easy the development of control software, do not have enough flexibility for intelligent generation systems connected to the grid. Programmable SoCs (System on Chip), like Zynq-7000, are very flexible devices that, however, do not have specific hardware for converters control, which hind the development of control software for this applications.

The most common process to generate control software for power converters is doing a model of the process. After doing this model the algorithm designed is implemented encoding it manually in the programming language wich has been decided to be used to program the controller in charge of the control. This process of manual encoding is slow, needs of high knowledge by the programmer and a big amount of  work. Currently, to solve the disadvantage of the manual encoding, there are several tools that allow us to generate code automatically. This tools reduce encoding errors, wich redound shorter development times.
In this work the student is going to develop the control software for an invertir using the development card ZedBoard Zynq-7000 ARM/FPGA SoC. The design of the control algorithm Will be verified through simulation with Simulink. And it Will be implemented in the SoC using the tool Simulink Coder to obtain the code through automatic generation from the developed algorithms in Simulink. Additional specific circuits that are necesary to drive the converter will be designed too.

The right performance of the controller will be verified through simulation with Simulink and the correct operation in the development card will be verified using the “Processor-in-the-loop (PIL)” technique.
Finally, it will be done a comparative with the implementation of the control algorithm in the development card H62C2 Concerto, that is based on a F28M366 micrcontroller.

<h2>Repository organization</h2>
The three pdf files in the root folder are the thesis, the A3 abstract and the presentation used in the project exposition. The folder "ModelosFinalesSimulink" contains all the final Simulink models used for the purpose of this project.
